/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var express = require('express');
var bodyParser = require('body-parser');
var SerialPort = require('serialport');
//var SerialPort = serialport.SerialPort;
var app = express();
app.use(bodyParser.json());
var dev;
if (process.argv[2] === 'dev') {
    dev = true;
} else {
    dev = false;
}

if (!dev) {
    var myPort = new SerialPort(process.argv[2], {
        baudRate: 250000,
        // look for return and newline at the end of each data packet:
        parser: SerialPort.parsers.readline("\n")
    });
}


app.get('/', function (req, res) {
    res.sendfile('index.html', {root: __dirname});
});

if (!dev){
    app.post('/rightup', function (req, res) {
        myPort.write("21\n");
        res.send(req.body);
    });

    app.post('/rightdown', function (req, res) {
        myPort.write("22\n");
        res.send(req.body);
    });

    app.post('/leftup', function (req, res) {
        myPort.write("11\n");
        res.send(req.body);
    });

    app.post('/leftdown', function (req, res) {
        myPort.write("12\n");
        res.send(req.body);
    });

    app.post('/right', function (req, res) {
        myPort.write("20\n");
        res.send(req.body);
    });

    app.post('/left', function (req, res) {
        myPort.write("10\n");
        res.send(req.body);
    });

    app.post('/up', function (req, res) {
        myPort.write("01\n");
        res.send(req.body);
    });

    app.post('/down', function (req, res) {
        myPort.write("02\n");
        res.send(req.body);
    });

    app.post('/stop', function (req, res) {
        myPort.write("00\n");
        res.send(req.body);
    });

    app.post('/coordinates', function (req, res) {

        if (!dev) {
            var body = req.body;
            if (body != null && body.hasOwnProperty('x') && body.hasOwnProperty('y')) {
                var data = body.x.toString() + body.y.toString() + "\n";
                console.log("data" + data + "x" + body.x + "y" + body.y);
                console.log("cls");
                myPort.write(data);

            }
            res.send(req.body);
        }

    });
}

app.listen(8082, function () {
    console.log('Listening on port 8082!');
});
