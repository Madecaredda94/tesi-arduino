#include <Servo.h>
#define dlay 25

String readString, servo1, servo2;
Servo myservo1;  // create servo object to control a servo 
Servo myservo2;

void setup() {
  Serial.begin(250000);
  myservo1.attach(14);  //the pin for the servo control 
  myservo2.attach(15);
  Serial.println("Setup complete"); 
  // put your setup code here, to run once:

}

void loop() {
    while (Serial.available()) {
    delay(3);  //delay to allow buffer to fill 
    if (Serial.available() >0) {
      char c = Serial.read();  //gets one byte from serial buffer
      readString += c; //makes the string readString
    } 
  }

  if (readString.length() >0) {
      Serial.println(readString); //see what was received
      
      
      servo1 = readString.substring(0, 1); //get the first character
      servo2 = readString.substring(1, 2); //get the next character
      
      int n1 = servo1.toInt();
      int n2 = servo2.toInt();

      if(n1==1){
        if(n2==1){
          
          movimento(120,120);
          
        } else if(n2==0){
          
          movimento(120,90);
          
        } else if(n2==2){
          
          movimento(120,60);
          
        }
      } else if (n1==0){
        if(n2==0){
          
          
          arresto();
          
        } else if (n2==1){
          
          movimento(90,120);
          
        } else if(n2==2){
          
          movimento(90,60);
        }
      } else if (n1==2){
        if (n2==0){
          
          movimento(60,90);
          
        } else if (n2==1){
          
          movimento(60,120);
          
        } else if (n2==2){
          
          movimento(60,60);
          
        }
      }
      
    readString="";
  } 
}

void movimento(int x1,int x2){
  partenza(x1,x2);
  delay(25);
  arresto();
  delay(25);
}

void partenza(int x1,int x2){
  myservo1.write(x1);
  myservo2.write(x2);
}

void arresto(){
  myservo1.write(90);
  myservo2.write(90);
}

